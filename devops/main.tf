
terraform {
  required_version = "1.0.4"
  backend "s3" {
    bucket         = "physis-terraform-state"
    key            = "implementations/physis-advisor-tf"
    region         = "us-east-1"
    dynamodb_table = "physis-advisor-tf-lock-table"
    profile        = "default"
  }
  required_providers {
    aws = ">= 3.56.0"
  }
}
locals {
  name           = "advisor_profile"
  env            = element(split("-", terraform.workspace), 0)
  region         = "us-east-1"
  aws_profile    = "default"
  account        = "655622384061"
  tags = {
    Project   = local.name,
    Env       = local.env,
    App       = local.name,
    CreatedBy = "terraform"
  }
}
provider "aws" {
  region  = local.region
  profile = local.aws_profile
}
