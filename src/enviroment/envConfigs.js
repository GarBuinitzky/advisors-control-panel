module.exports.envs = {

    "LambdaProperties_DEV": {
        FunctionName: "advisor-api",
        Runtime: "nodejs14.x",
        Role: "arn:aws:iam::655622384061:role/physisBackEnd-dev-us-east-1-lambdaRole",
        Handler: "lambda.handler",
        MemorySize: "256",
        Environment: {
            Variables: {
                Environment: "DEV",
                'COGNITO_USER_POOL_ID': 'us-east-1_9E1uJSY8z',
            }
        }
    },
    "LambdaProperties_TEST": {
        FunctionName: "advisor-api",
        Runtime: "nodejs14.x",
        Role: "arn:aws:iam::655622384061:role/physisBackEnd-dev-us-east-1-lambdaRole",
        Handler: "lambda.handler",
        MemorySize: "256",
        Environment: {
            Variables: {
                Environment: "TEST",
                'COGNITO_USER_POOL_ID': 'us-east-1_9E1uJSY8z',
            }
        }
    },
    "LambdaProperties_PROD": {
        FunctionName: "advisor-api",
        Runtime: "nodejs14.x",
        Role: "arn:aws:iam::655622384061:role/physisBackEnd-dev-us-east-1-lambdaRole",
        Handler: "lambda.handler",
        MemorySize: "256",
        Environment: {
            Variables: {
                Environment: "PROD",
                'COGNITO_USER_POOL_ID': 'us-east-1_mNGu20EY1',
            }
        }
    },
    "LambdaProperties_RAIFFEISEN_PROD": {
        FunctionName: "advisor-api",
        Runtime: "nodejs14.x",
        Role: "arn:aws:iam::655622384061:role/physisBackEnd-dev-us-east-1-lambdaRole",
        Handler: "lambda.handler",
        MemorySize: "256"
        ,
        Environment: {
            Variables: {
                Environment: "RAIFFEISEN_PROD",
                'COGNITO_USER_POOL_ID': 'us-east-1_udJtsRQdZ',
            }
        }
    },

};

