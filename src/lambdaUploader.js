const env = process.argv[3];
const isTopublis = (process.argv[2] === 'true');
console.log("isTopubliush = ", isTopublis);
const AWS = require('aws-sdk');
AWS.config.update({region: 'us-east-1'});
console.log(process.argv);
const S3 = require('aws-sdk/clients/s3');
const lambda = require('aws-sdk/clients/lambda');
const lambdaProperties = require('./enviroment/envConfigs').envs[`LambdaProperties_${env}`];
console.log("using lambdas properties ", lambdaProperties);
const fs = require('fs-extra');
const s3client = new S3();
const lambdaClient = new lambda();
const execSync = require('child_process').execSync;

console.log("remove /dist folder");

if (fs.existsSync("./dist")) {
    fs.removeSync('./dist');
}


execSync('npm install --only=prod', {encoding: 'utf-8'});
console.log("remove /dist folder completed");
console.log("building repository");
execSync('npm run build', {encoding: 'utf-8'});
console.log("building repository finished");
console.log("removing existing physis-profile-api.zip");

if (fs.existsSync("physis-profile-api.zip")) {
    fs.unlinkSync('physis-profile-api.zip');
}

console.log("removing existing physis-profile-api.zip completed");
execSync('zip -9 -r -q physis-profile-api.zip . -x \".git\" -x \".idea\" -x \"physis-profile-api.zip\"', {encoding: 'utf-8'});
console.log("compresion completed");
const fileContent = fs.readFileSync("physis-profile-api.zip");

// Setting up S3 upload parameters0

const updateLambda = async (Alias) => {
    console.log(`deployment on  ${Alias}`);
    const params = {
        Bucket: "physis-profile-api-bucket-deployment",
        Key: Alias + 'physis-profile-api.zip', // File name you want to save as in S3
        Body: fileContent
    };
    let s3UploadOK = true;
    const response = await s3client.upload(params).promise().catch((err) => {
        s3UploadOK = false;
        console.log("error uploading to s3", err);
    });
    if (!s3UploadOK) {
        console.log("s3 upload fails", s3UploadOK)
    }
    console.log("s3 upload completed");
    const getFunctionParams = {
        FunctionName: "physis-profile-api"
    };
    try {
        const getFunctionResponse = await lambdaClient.getFunction(getFunctionParams).promise().catch(async (err) => {
            console.log("function dont exist creating it");
            lambdaProperties.Code = {
                S3Bucket: params.Bucket,
                S3Key: params.Key
            };
            const createFunctionResepon = await lambdaClient.createFunction(lambdaProperties).promise();
            const CreateAlias = {
                FunctionName: "physis-profile-api",
                Name: Alias,
                FunctionVersion: createFunctionResepon.Version
            };
            const createAliasResponse = await lambdaClient.createAlias(CreateAlias).promise();

        });
        let updateFunctionResepon;
        const updateFunction = {
            FunctionName: "physis-profile-api",
            S3Bucket: params.Bucket,
            S3Key: params.Key,
            Publish: isTopublis
        };
        updateFunctionResepon = await lambdaClient.updateFunctionConfiguration(lambdaProperties).promise().catch(err => {
            console.log("error updating lambda configuration ", err)
        });
        updateFunctionResepon = await lambdaClient.updateFunctionCode(updateFunction).promise().catch(err => {
            console.log("error updating lambda code", err)
        });

        const getAliasParams = {
            FunctionName: "physis-profile-api",
            Name: Alias
        };

        const CreateAlias = {
            FunctionName: "physis-profile-api",
            Name: Alias,
            FunctionVersion: updateFunctionResepon.Version
        };

        try {
            const getAliasResponse = await lambdaClient.getAlias(getAliasParams).promise().catch(async (err) => {
                console.log("alias response dont exist");
                const createAliasResponse = await lambdaClient.createAlias(CreateAlias).promise();
            });
            if (getAliasResponse) {
                const updateAliasResponse = await lambdaClient.updateAlias(CreateAlias).promise();
            }

        } catch (e) {
            console.log(e)

        }

    } catch (e) {
        console.log(e)
    }
    console.log(response)

};

console.log("deploying on ", env);
updateLambda(env).then(() => {

    console.log("upload susccessfull")
});
