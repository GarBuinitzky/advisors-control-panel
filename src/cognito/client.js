const AWS = require('aws-sdk');
const to = require("../promiseParse");
const e = require("express");
AWS.config.update({
    region: "us-east-1",
});

const CognitoClient = new AWS.CognitoIdentityServiceProvider()

async function CreateAdvisor() {

    const UserAdvisor = {
        UserPoolId: "us-east-1_9E1uJSY8z",
        Username: "skateboardman9@hotmail.com",
        UserAttributes: [
            {
            Name: "email",
            Value: "skateboardman9@hotmail.com",
        },
         {
                Name: "custom:IsAdvisor",
                Value: "true"
         }
        ],

        ClientMetadata: {"custom:IsAdvisor": "1"},
    }


    const [resp, err] = await to(CognitoClient.adminCreateUser(UserAdvisor).promise())

    if (err != null) {
        console.log(err)
    }

}

async function GetAdvisor(email) {
    const GetUserInput = {
        UserPoolId: "us-east-1_9E1uJSY8z",
        Username: "f7bd081a-59db-4c36-97f2-ea8d05a0076e",
    }


    const [resp, err] = await to(CognitoClient.adminGetUser(GetUserInput).promise())

  console.log(resp)
}

async function UpdateAdvisorClientData() {

    const updateAttribute = {
        UserPoolId: "us-east-1_9E1uJSY8z",
        Username: "skateboardman9@hotmail.com",
        UserAttributes: [{
            Name: "email",
            Value: "juanvalero252@gmail.com",
        }],

        ClientMetadata: {"custom:IsAdvisor": "1"},
    }
    const [resp, err] = await to(CognitoClient.adminUpdateUserAttributes(updateAttribute).promise())

    console.log(resp,err)
}


module.exports = {UpdateAdvisorClientData,GetAdvisor,CreateAdvisor}
