const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');

const express = require('express');
const createError = require('http-errors');
const app = express();
const router = require('./routes/advisorRoutes');
const cors = require('cors');
const bodyParser = require('body-parser');


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(awsServerlessExpressMiddleware.eventContext());


app.use('/advisor', router);
console.log("configuration finish");
app.use(function (req, res, next) {
    next(createError(404));
});
// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.status(err.status || 500);
    res.send({
        message: err.message
    });
});

// Export your express server so you can import it in the lambda function.
module.exports = app;
