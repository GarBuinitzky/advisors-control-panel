const to = promise =>
    promise
        .then(data => [data,null])
        .catch(err => [null,err])


module.exports = to
