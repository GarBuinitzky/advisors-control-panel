process.env.Environment = "DEV";
const AWS = require("aws-sdk");
AWS.config.update({region: 'us-east-1'});
const app = require('./app');
const port = 8000;

const start = async () => {
    app.listen(port);
    console.log(`listening on http://localhost:${port}`);
};

start();

