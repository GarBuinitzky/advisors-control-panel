const path = require('path');


module.exports = {
    target: 'node',
    mode: "production",
    entry: "./src/lambda.js",
    output: {
        libraryTarget: 'commonjs',
        path: path.join(__dirname, '.webpack'),
        filename: '[name].js'
    },
    externals: {
        pg: 'commonjs pg',
        typeorm: 'commonjs typeorm'
    },
    stats: slsw.lib.webpack.isLocal
        ? 'errors-only'
        : 'normal',
    module: {
        rules: [
            {
                test: /\.js$/,
                include: [__dirname],
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true
                        }
                    }
                ]
            }
        ]
    }
}
